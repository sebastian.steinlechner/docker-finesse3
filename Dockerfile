FROM continuumio/anaconda3

# get build environment
RUN apt-get install -y build-essential

# clone Finesse3 repository into /opt/finesse3
WORKDIR /opt
RUN git clone https://git.ligo.org/finesse/finesse3.git

# update shell to use conda environment, install requirements for Finesse3
SHELL ["/bin/bash", "--login", "-c"]
WORKDIR /opt/finesse3
RUN conda activate base && \
    conda env update -f environment.yml

# build and install Finesse 3
RUN pip install -e .

# get Finesse 2
RUN conda install -c gwoptics pykat

# set up Jupyter Lab environment (WITHOUT login token, be careful!)
EXPOSE 8888
VOLUME /opt/notebooks
WORKDIR /opt/notebooks
#RUN jupyter notebook --generate-config && \
#    echo "c.NotebookApp.token=''" >> ~/.jupyter/jupyter_notebook_config.py

COPY bin/entrypoint.sh /usr/local/bin
ENTRYPOINT ["entrypoint.sh"]
#CMD ["/opt/conda/bin/jupyter", "lab", "--no-browser", "--ip=0.0.0.0", "--allow-root"]