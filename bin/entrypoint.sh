#!/bin/bash
# this is adapted from https://github.com/vemonet/Jupyterlab/

CMD="/opt/conda/bin/jupyter lab --no-browser --ip=0.0.0.0 --allow-root"

if [[ -v JUPYTER_PASSWORD ]]; then
    JUPYTER_PASSWORD=$(python -c "import IPython; print(IPython.lib.security.passwd('$JUPYTER_PASSWORD'))")
    CMD="$CMD --NotebookApp.token='' --NotebookApp.password='${JUPYTER_PASSWORD}'"
fi

echo
echo "Installed software"
echo "=================="
python --version
pip --version
jupyter --version

echo
exec $CMD "$@"
